After reading this article [DoubleU-Net: A Deep Convolutional NeuralNetwork for Medical Image Segmentation](https://arxiv.org/pdf/2006.04868.pdf), I wanted to implement Double Unet and compare its performance to a simple Unet.

To do so, we will use the retinal vessel segmentation problem.
We will train our network on the [DRIVE](https://drive.grand-challenge.org/) dataset and compare them on the [STARE](  https://cecas.clemson.edu/~ahoover/stare/probing/index.html) dataset.

# Unet


![](https://miro.medium.com/max/2824/1*f7YOaE4TWubwaFF7Z1fzNw.png)U-net is a convolutional network well known for his performance in segmentation task.

It consists of the repeated application of two 3x3 convolutions (unpadded convolutions), each followed by a rectified linear unit (ReLU) and a 2x2 max pooling operation with stride 2 for downsampling. 
At each downsampling step we double the number of feature channels. Every step in the expansive path consists of an upsampling of the feature map followed by a 2x2 convolution (“up-convolution”) that halves the number of feature channels, a concatenation with the correspondingly cropped feature map from the contracting path, and two 3x3 convolutions, each followed by a ReLU. 

The cropping is necessary due to the loss of border pixels in every convolution. At the final layer a 1x1 convolution is used to map each 64-component feature vector to the desired number of classes. In total the network has 23 convolutional layers.

Source: [](http://arxiv.org/abs/1505.04597v1) [U-Net: Convolutional Networks for Biomedical Image Segmentation](http://arxiv.org/abs/1505.04597v1)
  
  
  
  
  

# Double Unet :
![](https://i.ibb.co/KyXDQwV/Double-U-Net.png)

The model consists of a VGG19 pretrined sub-network as an encoder, trained on imagenet and a custom decoder sub-network which forms the First U-net Network (NETWORK1). The Second U-net (NETWORK2) consists of a element-wise image-mask multiplier, custom encoder blocks and custom decoder blocks.

VGG and custom encoder blocker encodes the information contained in the input image. Each encoder block in the NETWORK2 performs two 3×3 convolution operation, each followed by a batch normalization. The batch normalization reduces the internal co-variant shift and also regularizes the model. A Rectified Linear Unit (ReLU) activation function is applied, which introduces non-linearity into the model. 

This is followed by a squeeze-and- excitation block, which enhances the quality of the feature maps and max-pooling with a 2×2 window and stride 2 to reduce the spatial dimension of the feature maps. [Squeeze-and-Excitation Networks](https://arxiv.org/pdf/1709.01507.pdf)

Atrous SpatialPyramid Pooling (ASPP) is used in both the sub-networks between the encoder and the decoder to extract high-resolution feature maps that lead to superior performance. [Rethinking Atrous Convolution for Semantic Image Segmentation](https://arxiv.org/pdf/1706.05587.pdf)

Decoder blocks uses Conv2DTranspose layers which learns a number of filters for performing the upsizing specified with the appropriate kernel_size. The decoder in the NETWORK1,uses only skip connection from the first encoder but, in the decoder of NETWORK2, uses skip connection from both the encoders, which maintains the spatial resolution and enhances the quality of the output feature maps. Squeeze-and-excite blocks are used in the decoder blocks of NETWORK1 and NETWORK2 which helps in reducing redundant information.

The output masks from both the networks are concatenated, then a final conv layer is used to combine both the masks to get the final output mask.

The intermediate concatenation and the multiplications of the input image with the output of NETWORK1 and then again the concatenation with the output of NETWORK2 improves the performance of the network and this is the intuitive basis and motivation behind this architecture as described in the original paper.
  
  Source : [DoubleU-Net: A Deep Convolutional NeuralNetwork for Medical Image Segmentation](https://arxiv.org/pdf/2006.04868.pdf)
  
  

# Performance on STARE dataset :

I also implemented other Unet type networks on Other_Unet(r2).ipynb and the r2 was the best.

|| dice_coeff|recall|precision|AUC
--- |--- |--- |--- |--- |
|U-net|0.6716| 0.7799|0.7468 |0.9608|
|DoubleU-Net |0.7191 |0.8394|0.7141|0.9630|
|r2_U-net|0.7128| 0.7480 |0.7598 |0.9342|

  
We can see a big improvement in terms of dice coefficient, DoubleU-Net is even better than r2_U-net.
But there is also a trade-off between recall and precision.
DoubleU-Net has better recall while U-Net has better precision.

So, in our example, Double Unet performs better but not as much as we might expected.
